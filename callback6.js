/*
Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for all lists simultaneously
*/
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const callback6 = (boardsData, listsData, cardsData, boardName, callback) => {
  setTimeout(() => {
    if (typeof boardName !== "string") {
      callback(new Error("\nInvalid Input\n", null));
    } else {
      const getBoard = boardsData.find((board) => boardName === board.name);

      if (getBoard !== undefined) {
        callback1(boardsData, getBoard.id, (error, board) => {
          if (error) {
            console.error(error);
          } else {
            console.log(board);

            callback2(listsData, board.id, (error, allListsInBoard) => {
              if (error) {
                console.log(error);
              } else {
                console.log(allListsInBoard);

                allListsInBoard[1].map((list) => {
                  callback3(cardsData, list.id, (error, card) => {
                    if (error) {
                      console.error(error);
                    } else {
                      console.log(card);
                    }
                  });
                });
              }
            });
          }
        });
      } else {
        callback(new Error(`\nno board with name "${boardName}"\n`, null));
      }
    }
  }, 2000);
};

module.exports = callback6;
