const callback1 = require("../callback1");
const boardsData = require("../data/boards.json");

const callback = (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
};

// added setTimeout in test cases to add delay.

let boardID1 = "mcu453ed";

callback1(boardsData, boardID1, callback); //got desired output no errors.

let boardID2 = "dsdd";

setTimeout(() => callback1(boardsData, boardID2, callback), 6000); // got desired output 'board not found' error.

let boardID3 = 52;

setTimeout(() => callback1(boardsData, boardID3, callback), 9000); // got desired output 'Invalid Input' error.
