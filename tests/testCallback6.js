const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

const callback6 = require("../callback6");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

const boardName1 = "Thanos";

callback6(boardsData, listsData, cardsData, boardName1, callback);

const boardName2 = "Darkseid";

setTimeout(
  () => callback6(boardsData, listsData, cardsData, boardName2, callback),
  8000
);

const boardName3 = "test";

setTimeout(
  () => callback6(boardsData, listsData, cardsData, boardName3, callback),
  20000
);

const boardName4 = 11;

setTimeout(
  () => callback6(boardsData, listsData, cardsData, boardName4, callback),
  20000
);
