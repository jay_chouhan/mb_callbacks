const callback2 = require("../callback2");
const listsData = require("../data/lists.json");

const callback = (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
};

// added setTimeout in test cases to give delay.

let boardId1 = "mcu453ed";

callback2(listsData, boardId1, callback); // got desired output no errors.

let boardId2 = "";

setTimeout(() => callback2(listsData, boardId2, callback), 4000); // got desired output 'list not found' error.

let boardId3 = 10;

setTimeout(() => callback2(listsData, boardId3, callback), 7000); // got desired output 'Invalid Input' error.
