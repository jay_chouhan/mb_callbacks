const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

const callback4 = require("../callback4");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

const boardName = "Thanos";
const listName = "Mind";

callback4(boardsData, listsData, cardsData, boardName, listName, callback);

const boardName1 = "xyx";

setTimeout(
  () =>
    callback4(boardsData, listsData, cardsData, boardName1, listName, callback),
  8000
);

const listName1 = "not mind";

setTimeout(
  () =>
    callback4(boardsData, listsData, cardsData, boardName, listName1, callback),
  12000
);

const boardName2 = 10;

setTimeout(
  () =>
    callback4(boardsData, listsData, cardsData, boardName2, listName, callback),
  18000
);
