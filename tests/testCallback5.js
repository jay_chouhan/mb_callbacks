const boardsData = require("../data/boards.json");
const listsData = require("../data/lists.json");
const cardsData = require("../data/cards.json");

const callback5 = require("../callback5");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

const boardName = "Thanos";
const listName1 = "Mind";
const listName2 = "Space";

callback5(
  boardsData,
  listsData,
  cardsData,
  boardName,
  listName1,
  listName2,
  callback
);

const boardName1 = "test";

setTimeout(
  () =>
    callback5(
      boardsData,
      listsData,
      cardsData,
      boardName1,
      listName1,
      listName2,
      callback
    ),
  8000
);

const boardName2 = 11;

setTimeout(
  () =>
    callback5(
      boardsData,
      listsData,
      cardsData,
      boardName2,
      listName1,
      listName2,
      callback
    ),
  12000
);
