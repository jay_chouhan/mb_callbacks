const callback3 = require("../callback3");
const cardsData = require("../data/cards.json");

const callback = (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
};

// added setTimeout in test cases to give delay.

let listId1 = "qwsa221";

callback3(cardsData, listId1, callback); //got desired output no errors.

let listId2 = "";

setTimeout(() => callback3(cardsData, listId2, callback), 4000); // got desired output 'card not found' error.

let listId3 = 10;

setTimeout(() => callback3(cardsData, listId3, callback), 7000); //got desired output 'Invalid Input' error.
