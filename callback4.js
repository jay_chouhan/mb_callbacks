/*
  Get information from the Thanos boards
  Get all the lists for the Thanos board
  Get all cards for the Mind list simultaneously
*/
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const callback4 = (
  boardsData,
  listsData,
  cardsData,
  boardName,
  listName,
  callback
) => {
  setTimeout(() => {
    if (typeof boardName !== "string" || typeof listName !== "string") {
      callback(new Error("\nInvalid Input\n", null));
    } else {
      const getBoard = boardsData.find((board) => boardName === board.name);

      if (getBoard !== undefined) {
        callback1(boardsData, getBoard.id, (error, board) => {
          if (error) {
            console.error(error);
          } else {
            console.log(board);

            callback2(listsData, getBoard.id, (error, list) => {
              if (error) {
                console.log(error);
              } else {
                console.log(getBoard.name);
                console.log(list);

                const getlist = list[1].find(
                  (listId) => listId.name === listName
                );

                if (getlist !== undefined) {
                  callback3(cardsData, getlist.id, (error, card) => {
                    if (error) {
                      console.error(error);
                    } else {
                      console.log(getlist.name);
                      console.log(card);
                    }
                  });
                } else {
                  callback(
                    new Error(`\nno list with name "${listName}"\n`, null)
                  );
                }
              }
            });
          }
        });
      } else {
        callback(new Error(`\nno board with name "${boardName}"\n`, null));
      }
    }
  }, 2000);
};

module.exports = callback4;
