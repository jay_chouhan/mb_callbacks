//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const callback3 = (cardsData, listId, callback) => {
  setTimeout(() => {
    console.log("\nfetching card data...");

    if (
      typeof cardsData !== "object" ||
      typeof listId !== "string" ||
      typeof callback !== "function"
    ) {
      callback(new Error("\n\nInvalid input\n", null));
    } else {
      const getList = Object.entries(cardsData).find(
        (card) => listId === card[0]
      );

      if (getList === undefined) {
        callback(new Error("\n\ncard not found\n", null));
      } else {
        callback(null, getList);
      }
    }
  }, 2000);
};

module.exports = callback3;
