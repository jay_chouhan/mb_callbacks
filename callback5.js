/*
    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");

const callback5 = (
  boardsData,
  listsData,
  cardsData,
  boardName,
  listName1,
  listName2,
  callback
) => {
  setTimeout(() => {
    if (
      typeof boardName !== "string" ||
      typeof listName1 !== "string" ||
      typeof listName2 !== "string"
    ) {
      callback(new Error("\nInvalid Input\n", null));
    } else {
      const getBoard = boardsData.find((board) => boardName === board.name);

      if (getBoard !== undefined) {
        callback1(boardsData, getBoard.id, (error, board) => {
          if (error) {
            console.error(error);
          } else {
            console.log(board);

            callback2(listsData, board.id, (error, allListsInBoard) => {
              if (error) {
                console.error(error);
              } else {
                console.log();
                console.log(allListsInBoard);

                allListsInBoard[1]
                  .filter(
                    (list) => list.name === listName1 || list.name === listName2
                  )
                  .map((list) => {
                    callback3(cardsData, list.id, (error, card) => {
                      if (error) {
                        console.error(error);
                      } else {
                        console.log(list.name);
                        console.log(card);
                      }
                    });
                  });
              }
            });
          }
        });
      } else {
        callback(new Error(`no board with name "${boardName}"`, null));
      }
    }
  }, 2000);
};

module.exports = callback5;
