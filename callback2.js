//Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.

const callback2 = (listsData, boardId, callback) => {
  setTimeout(() => {
    console.log("\nfetching list data...");

    if (
      typeof listsData !== "object" ||
      typeof boardId !== "string" ||
      typeof callback !== "function"
    ) {
      callback(new Error("\n\nInvalid input\n", null));
    } else {
      const getList = Object.entries(listsData).find(
        (list) => boardId === list[0]
      );

      if (getList === undefined) {
        callback(new Error("\n\nlist not found\n", null));
      } else {
        callback(null, getList);
      }
    }
  }, 2000);
};

module.exports = callback2;
