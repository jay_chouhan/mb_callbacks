// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.

const callback1 = (boardsData, boardId, callback) => {
  setTimeout(() => {
    console.log("fetching board data...");

    if (
      !Array.isArray(boardsData) ||
      typeof boardId !== "string" ||
      typeof callback !== "function"
    ) {
      callback(new Error("\n\nInvalid Input\n", null));
    } else {
      const getBoardId = boardsData.find((board) => board.id === boardId);

      if (getBoardId === undefined) {
        callback(new Error("\n\nboard not found\n", null));
      } else {
        callback(null, getBoardId);
      }
    }
  }, 2000);
};

module.exports = callback1;
